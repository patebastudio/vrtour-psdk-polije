using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovePoint : MonoBehaviour
{
    public GameObject xrRig;
    public Transform[] targetObjects;

    public void MoveXRigToTargetObject(int moveTo)
    {
        Transform targetObject = targetObjects[moveTo];

        Vector3 newPosition = new Vector3(targetObject.position.x, xrRig.transform.position.y, targetObject.position.z);
        xrRig.transform.position = newPosition;
    }
}
