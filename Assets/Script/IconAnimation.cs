using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IconAnimation : MonoBehaviour
{
    [Tooltip("The tag to identify the view finder object.")]
    public string viewFinderTag;
    [Tooltip("Reference to the Animator component.")]
    public Animator animator;

    private bool onSightRange = false; // Track collision state.

    private void Start()
    {
        if (animator == null)
        {
            animator = GetComponent<Animator>();

            if (animator == null)
            {
                Debug.LogError("Animator component not found. Disabling AnimationOnCollision script.");
                enabled = false;
            }
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(viewFinderTag))
        {
            onSightRange = true;
            Debug.Log("detected");
            animator.SetBool("onSightRange", onSightRange);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag(viewFinderTag))
        {
            onSightRange = false;
            Debug.Log("left");
            animator.SetBool("onSightRange", onSightRange);
        }
    }
}
